﻿using System;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

// See - https://docs.microsoft.com/en-us/dotnet/framework/network-programming/asynchronous-server-socket-example

namespace AsyncSocketServer.WinForms
{
    public partial class MainForm : Form
    {
        private readonly string _userDetails;

        //-----------------------------------------------------------------------------------------

        public MainForm()
        {
            InitializeComponent();

            var v = Assembly.GetExecutingAssembly().GetName().Version;

            Lbl_Main_Version.Text = $@"Version {v.Major}.{v.Minor}.{v.Build:00}.{v.Revision:00000}";

            Console.WriteLine(@"[Form_Main]  Initialization started");

            _userDetails = GetUserDetails();

            // AsynchronousSocketListener.StartListening();
        }

        //=========================================================================================
        // Form Handlers
        //-----------------------------------------------------------------------------------------

        #region - Form Handlers

        //-----------------------------------------------------------------------------------------

        private void MainForm_Load(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            // Invoke(new Action(() => { MessageBox.Show(this, @"Be patient. I am busy seeing whhat's what.  I'll be ready for you a little while!", @"PWI Starting"); }));

            RtbConsole.Setup(Rtb_Console);

            Txt_Server.Text = Globals.Hostname;
            Txt_Port.Text = Globals.Port.ToString();

            var ipHostInfo = Dns.GetHostEntry(Globals.Hostname); // IPHostEntry
            var ipAddress = ipHostInfo.AddressList[0]; // IPAddress

            Txt_Address.Text = ipAddress.ToString();

            Cursor = Cursors.Default;

        } // MainForm_Load

        //-----------------------------------------------------------------------------------------

        #endregion - Form Handlers

        //=========================================================================================
        // Button Handlers
        //-----------------------------------------------------------------------------------------

        #region - Button Handlers

        //-----------------------------------------------------------------------------------------

        private void Btn_Start_Click(object sender, EventArgs e)
        {
            Start();

        } // Btn_Start_Click

        //-----------------------------------------------------------------------------------------

        private void Btn_Stop_Click(object sender, EventArgs e)
        {
            Stop();

        } // Btn_Stop_Click

        //-----------------------------------------------------------------------------------------

        private void Btn_Exit_Click(object sender, EventArgs e)
        {
            Exit();

        } // Btn_Exit_Click

        //-----------------------------------------------------------------------------------------

        #endregion - Button Handlers

        //=========================================================================================
        // Actions
        //-----------------------------------------------------------------------------------------

        #region - Actions

        //-----------------------------------------------------------------------------------------

        internal void Start()
        {
            var listeningThread = new Thread(() => AsynchronousSocketListener.StartListening(this));

            listeningThread.Start();

            Btn_Start.Enabled = false;
            Btn_Stop.Enabled = true;

        } // Start

        //-----------------------------------------------------------------------------------------

        internal void Stop()
        {
            AsynchronousSocketListener.StopListening();

            Btn_Start.Enabled = true;
            Btn_Stop.Enabled = false;

        } // Stop

        //-----------------------------------------------------------------------------------------

        internal void Exit()
        {
            Application.Exit();

        } // Exit

        #endregion - Actions

        //=========================================================================================
        // Classify and add to appropriate region
        //-----------------------------------------------------------------------------------------

        #region - Classify these

        //-----------------------------------------------------------------------------------------

        public string GetUserDetails()
        {
            var machineName = Environment.MachineName;

            var userDisplayName = System.DirectoryServices.AccountManagement.UserPrincipal.Current.DisplayName;

            var localUsername = Environment.UserName;

            var identity = System.Security.Principal.WindowsIdentity.GetCurrent();

            var networkUsername = identity.Name;

            Console.WriteLine(@"[Form_Main]  Initialization completed");
            Console.WriteLine(@"[Form_Main]  Machine Name {0}", machineName);
            Console.WriteLine(@"[Form_Main]  User Display Name {0}", userDisplayName);
            Console.WriteLine(@"[Form_Main]  Local Username {0}", localUsername);
            Console.WriteLine(@"[Form_Main]  Network Username {0}", networkUsername);

            return $@"{networkUsername ?? localUsername}@{machineName}";

        } // GetUserDetails


        //-----------------------------------------------------------------------------------------

        #endregion - Classify these

        //=========================================================================================
        // Various delegates for updating the UI
        //-----------------------------------------------------------------------------------------

        // http://stackoverflow.com/questions/661561/how-to-update-the-gui-from-another-thread-in-c
        // http://stackoverflow.com/questions/570537/update-label-while-processing-in-windows-forms/570557#570557
        // http://stackoverflow.com/questions/1360944/force-gui-update-from-ui-thread
        // http://tech.pro/tutorial/800/working-with-the-wpf-dispatcher
        // http://stackoverflow.com/questions/714666/is-it-appropriate-to-extend-control-to-provide-consistently-safe-invoke-begininv

        #region Delegates

        //=========================================================================================

        private delegate void LogConsoleDelegate(string text);

        //-----------------------------------------------------------------------------------------

        public void WriteLogConsole(string text)
        {
            if (InvokeRequired)
            {
                Invoke(new LogConsoleDelegate(WriteLogConsole), text);
                return;
            }

            RtbConsole.WriteLine(text);

        } // WriteLogConsole

        //=========================================================================================

        private void UpdateDisplay(string line, string results)
        {
            Refresh();

        } // UpdateDisplay

        //-----------------------------------------------------------------------------------------

        public delegate void UpdateGuiDelegate(string text, string results);

        //-----------------------------------------------------------------------------------------

        public void UpdateGui(string text, string results)
        {
            Invoke(new UpdateGuiDelegate(UpdateDisplay), text, results);

        } // UpdateGui

        //-----------------------------------------------------------------------------------------
        
        #endregion Delegates
        
        //=========================================================================================


    }
}
