﻿namespace AsyncSocketServer.WinForms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Btn_Exit = new System.Windows.Forms.Button();
            this.Rtb_Console = new System.Windows.Forms.RichTextBox();
            this.Btn_Start = new System.Windows.Forms.Button();
            this.Btn_Stop = new System.Windows.Forms.Button();
            this.Lbl_Main_Version = new System.Windows.Forms.Label();
            this.Txt_Port = new System.Windows.Forms.TextBox();
            this.Lbl_Port = new System.Windows.Forms.Label();
            this.Txt_Address = new System.Windows.Forms.TextBox();
            this.Lbl_Address = new System.Windows.Forms.Label();
            this.Txt_Server = new System.Windows.Forms.TextBox();
            this.Lbl_Server = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Btn_Exit
            // 
            this.Btn_Exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Exit.Location = new System.Drawing.Point(713, 415);
            this.Btn_Exit.Name = "Btn_Exit";
            this.Btn_Exit.Size = new System.Drawing.Size(75, 23);
            this.Btn_Exit.TabIndex = 0;
            this.Btn_Exit.Text = "Exit";
            this.Btn_Exit.UseVisualStyleBackColor = true;
            this.Btn_Exit.Click += new System.EventHandler(this.Btn_Exit_Click);
            // 
            // Rtb_Console
            // 
            this.Rtb_Console.Location = new System.Drawing.Point(12, 38);
            this.Rtb_Console.Name = "Rtb_Console";
            this.Rtb_Console.ReadOnly = true;
            this.Rtb_Console.Size = new System.Drawing.Size(776, 371);
            this.Rtb_Console.TabIndex = 1;
            this.Rtb_Console.TabStop = false;
            this.Rtb_Console.Text = "";
            // 
            // Btn_Start
            // 
            this.Btn_Start.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Start.Location = new System.Drawing.Point(632, 415);
            this.Btn_Start.Name = "Btn_Start";
            this.Btn_Start.Size = new System.Drawing.Size(75, 23);
            this.Btn_Start.TabIndex = 2;
            this.Btn_Start.Text = "Start";
            this.Btn_Start.UseVisualStyleBackColor = true;
            this.Btn_Start.Click += new System.EventHandler(this.Btn_Start_Click);
            // 
            // Btn_Stop
            // 
            this.Btn_Stop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Stop.Enabled = false;
            this.Btn_Stop.Location = new System.Drawing.Point(551, 415);
            this.Btn_Stop.Name = "Btn_Stop";
            this.Btn_Stop.Size = new System.Drawing.Size(75, 23);
            this.Btn_Stop.TabIndex = 3;
            this.Btn_Stop.Text = "Stop";
            this.Btn_Stop.UseVisualStyleBackColor = true;
            this.Btn_Stop.Click += new System.EventHandler(this.Btn_Stop_Click);
            // 
            // Lbl_Main_Version
            // 
            this.Lbl_Main_Version.AutoSize = true;
            this.Lbl_Main_Version.Location = new System.Drawing.Point(692, 15);
            this.Lbl_Main_Version.Name = "Lbl_Main_Version";
            this.Lbl_Main_Version.Size = new System.Drawing.Size(83, 13);
            this.Lbl_Main_Version.TabIndex = 4;
            this.Lbl_Main_Version.Text = "V0.0.000.00000";
            // 
            // Txt_Port
            // 
            this.Txt_Port.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Txt_Port.Location = new System.Drawing.Point(459, 12);
            this.Txt_Port.Name = "Txt_Port";
            this.Txt_Port.Size = new System.Drawing.Size(154, 20);
            this.Txt_Port.TabIndex = 19;
            // 
            // Lbl_Port
            // 
            this.Lbl_Port.AutoSize = true;
            this.Lbl_Port.Location = new System.Drawing.Point(427, 15);
            this.Lbl_Port.Name = "Lbl_Port";
            this.Lbl_Port.Size = new System.Drawing.Size(26, 13);
            this.Lbl_Port.TabIndex = 18;
            this.Lbl_Port.Text = "Port";
            // 
            // Txt_Address
            // 
            this.Txt_Address.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Txt_Address.Location = new System.Drawing.Point(267, 12);
            this.Txt_Address.Name = "Txt_Address";
            this.Txt_Address.Size = new System.Drawing.Size(154, 20);
            this.Txt_Address.TabIndex = 17;
            // 
            // Lbl_Address
            // 
            this.Lbl_Address.AutoSize = true;
            this.Lbl_Address.Location = new System.Drawing.Point(216, 15);
            this.Lbl_Address.Name = "Lbl_Address";
            this.Lbl_Address.Size = new System.Drawing.Size(45, 13);
            this.Lbl_Address.TabIndex = 16;
            this.Lbl_Address.Text = "Address";
            // 
            // Txt_Server
            // 
            this.Txt_Server.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Txt_Server.Location = new System.Drawing.Point(56, 12);
            this.Txt_Server.Name = "Txt_Server";
            this.Txt_Server.Size = new System.Drawing.Size(154, 20);
            this.Txt_Server.TabIndex = 15;
            // 
            // Lbl_Server
            // 
            this.Lbl_Server.AutoSize = true;
            this.Lbl_Server.Location = new System.Drawing.Point(12, 15);
            this.Lbl_Server.Name = "Lbl_Server";
            this.Lbl_Server.Size = new System.Drawing.Size(38, 13);
            this.Lbl_Server.TabIndex = 14;
            this.Lbl_Server.Text = "Server";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Txt_Port);
            this.Controls.Add(this.Lbl_Port);
            this.Controls.Add(this.Txt_Address);
            this.Controls.Add(this.Lbl_Address);
            this.Controls.Add(this.Txt_Server);
            this.Controls.Add(this.Lbl_Server);
            this.Controls.Add(this.Lbl_Main_Version);
            this.Controls.Add(this.Btn_Stop);
            this.Controls.Add(this.Btn_Start);
            this.Controls.Add(this.Rtb_Console);
            this.Controls.Add(this.Btn_Exit);
            this.Name = "MainForm";
            this.Text = "Async Socket Server";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_Exit;
        private System.Windows.Forms.RichTextBox Rtb_Console;
        private System.Windows.Forms.Button Btn_Start;
        private System.Windows.Forms.Button Btn_Stop;
        private System.Windows.Forms.Label Lbl_Main_Version;
        private System.Windows.Forms.TextBox Txt_Port;
        private System.Windows.Forms.Label Lbl_Port;
        private System.Windows.Forms.TextBox Txt_Address;
        private System.Windows.Forms.Label Lbl_Address;
        private System.Windows.Forms.TextBox Txt_Server;
        private System.Windows.Forms.Label Lbl_Server;
    }
}

