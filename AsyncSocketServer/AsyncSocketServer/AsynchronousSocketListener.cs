﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using AsyncSocketServer.WinForms;

namespace AsyncSocketServer
{
    class AsynchronousSocketListener
    {
        internal static bool loop;
        internal static MainForm ParentForm;

        // Thread signal - https://docs.microsoft.com/en-us/dotnet/api/system.threading.manualresetevent

        public static ManualResetEvent allDone = new ManualResetEvent(false);

        //-----------------------------------------------------------------------------------------

        public static void StartListening(MainForm form)
        {
            ParentForm = form;

            // Establish the local endpoint for the socket.
            // The DNS name of the computer running the listener is "localhost".

            ParentForm.WriteLogConsole($@"Listener started on {Globals.Hostname}");

            var ipHostInfo = Dns.GetHostEntry(Globals.Hostname); // IPHostEntry
            var ipAddress = ipHostInfo.AddressList[0]; // IPAddress
            var localEndPoint = new IPEndPoint(ipAddress, Globals.Port); // IPEndPoint

            ParentForm.WriteLogConsole($@"Listening on {ipAddress}");

            loop = true;

            // Create a TCP/IP socket.

            var listener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            var lo = new LingerOption(false, 0);

            listener.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Linger, lo);

            // Bind the socket to the local endpoint and listen for incoming connections.

            Console.WriteLine(@"Started listening");

            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(100);

                while (loop)
                {
                    // Set the event to nonsignaled state.

                    allDone.Reset();

                    // Start an asynchronous socket to listen for connections.

                    ParentForm.WriteLogConsole(@"Waiting for a connection...");

                    listener.BeginAccept(AcceptCallback, listener);

                    // Wait until a connection is made before continuing.

                    allDone.WaitOne();
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                listener.Close();
                // listener.Shutdown(SocketShutdown.Both);
                // listener.Disconnect(true);
            }

            ParentForm.WriteLogConsole(@"Listener shut down...");

            Console.WriteLine(@"\nPress ENTER to continue...");
            Console.Read();

        } // StartListening

        //-----------------------------------------------------------------------------------------

        public static void AcceptCallback(IAsyncResult ar)
        {
            // Signal the main thread to continue.

            allDone.Set();

            // Get the socket that handles the client request.

            if (!loop) return;

            var listener = (Socket) ar.AsyncState;
            var handler = listener.EndAccept(ar);

            // Create the state object.

            var state = new StateObject {workSocket = handler};

            handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, ReadCallback, state);

        } // AcceptCallback

        //-----------------------------------------------------------------------------------------

        public static void ReadCallback(IAsyncResult ar)
        {
            // Retrieve the state object and the handler socket from the asynchronous state object.

            var state = (StateObject) ar.AsyncState; // StateObject

            var handler = state.workSocket; // Socket

            // Read data from the client socket.
            // Note: This breaks when the far end drops out! - Wrap in a try {} except {}
 
            var bytesRead = handler.EndReceive(ar);

            if (bytesRead > 0)
            {
                // There  might be more data, so store the data received so far.

                state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));

                // Check for end-of-file tag. If it is not there, read more data.

                var content = state.sb.ToString();

                if (content.IndexOf(Globals.EoT, StringComparison.Ordinal) > -1)
                {
                    // All the sent data has been read - now display it and echo it.

                    var msg = $@"Read {content.Length} bytes from socket. {Environment.NewLine}Data |{content}|";

                    Console.WriteLine(msg);
                    ParentForm.WriteLogConsole(msg);

                    Send(handler, content);
                }
                else
                {
                    // Have not received the end of transmission marker - so more to come.

                    handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, ReadCallback, state);
                }
            }

        } // ReadCallback

        //-----------------------------------------------------------------------------------------

        private static void Send(Socket handler, string data)
        {
            // Convert the string to byte data using ASCII encoding.

            var byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote client.

            handler.BeginSend(byteData, 0, byteData.Length, 0, SendCallback, handler);

        } // Send

        //-----------------------------------------------------------------------------------------

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.

                var handler = (Socket) ar.AsyncState; // Socket

                // Complete sending the data to the remote device.

                var bytesSent = handler.EndSend(ar);

                Console.WriteLine($@"Sent {bytesSent} bytes to client.");

                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

        } // SendCallback
        
        //-----------------------------------------------------------------------------------------

        public static void StopListening()
        {
            loop = false;

            RtbConsole.WriteLine(@"Stopping Listener");

            allDone.Set();

        } // StopListening

        //-----------------------------------------------------------------------------------------


    }

}