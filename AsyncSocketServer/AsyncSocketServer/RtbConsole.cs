﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace AsyncSocketServer
{
    public class RtbConsole
    {
        private static RichTextBox _textArea;

        //---------------------------------------------------------------------

        public static void Setup(RichTextBox textBox)
        {
            _textArea = textBox;

            textBox.Font = new Font(FontFamily.GenericMonospace, 10, FontStyle.Regular);

        } // Setup

        //---------------------------------------------------------------------

        public static void Write(string text)
        {
            _textArea.AppendText(text);

            _textArea.SelectionStart = _textArea.Text.Length; // Set the current caret position at the end

            _textArea.ScrollToCaret();

        } // Write

        //---------------------------------------------------------------------

        public static void Write(string text, Color colour)
        {
            _textArea.SelectionColor = colour;

            _textArea.AppendText(text);

            _textArea.SelectionColor = Color.Black;

        } // Write

        //---------------------------------------------------------------------

        public static void WriteLine(string text = "", int mask = 0)
        {
            if ((mask & 1) == 1) _textArea.AppendText(Environment.NewLine);

            _textArea.AppendText(text);
            _textArea.AppendText(Environment.NewLine);

            if ((mask & 2) == 2) _textArea.AppendText(Environment.NewLine);

            _textArea.Update();

            _textArea.SelectionStart = _textArea.Text.Length; //Set the current caret position at the end

            _textArea.ScrollToCaret();

            _textArea.Refresh();

        } // WriteLine

        //---------------------------------------------------------------------

        public static void WriteLine(string text, Color colour, int mask = 0)
        {
            _textArea.SelectionColor = colour;

            WriteLine(text, mask: mask);

            _textArea.SelectionColor = Color.Black;

        } // WriteLine

        //---------------------------------------------------------------------

        public static void WriteWarning(string text = "")
        {
            WriteLine(text, Color.Orange);

        } // WriteWarning

        //---------------------------------------------------------------------

        public static void WriteError(string text = "")
        {
            WriteLine(text, Color.Red);

        } // WriteError

        //---------------------------------------------------------------------

        public static void WriteFinished(string text = "")
        {
            WriteLine(text, Color.Green);

        } // WriteError

        //---------------------------------------------------------------------

        public static void Info(string text = "")
        {
            WriteLine(text, Color.Purple);

        } // WriteError

        //---------------------------------------------------------------------

        public static void Clear()
        {
            _textArea.Text = string.Empty;

            _textArea.SelectionStart = _textArea.Text.Length; //Set the current caret position at the end

            _textArea.ScrollToCaret();

        } // Clear

        //---------------------------------------------------------------------

        public static void SetFocus()
        {
            _textArea.Focus();

            _textArea.SelectionStart = _textArea.Text.Length;
            _textArea.SelectionLength = 0;

        } // SetFocus

        //---------------------------------------------------------------------

        public static void SetFocusToEnd()
        {
            _textArea.Focus();

            _textArea.SelectionStart = _textArea.Text.Length;
            _textArea.SelectionLength = 0;

        } // WriteLine

        //---------------------------------------------------------------------

    } // class RtbConsole

    //-------------------------------------------------------------------------
}
