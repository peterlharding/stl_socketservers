﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;

namespace SimpleSocketServer
{
    public partial class MainForm : Form
    {

        private readonly List<Socket> clients = new List<Socket>();
        private TcpListener listener;


        public MainForm()
        {
            InitializeComponent();

            var listeningThread = new Thread(new ThreadStart(ListeningThread));
            listeningThread.Start();

        } // MainForm

        private void Btn_Exit_Click(object sender, EventArgs e)
        {
            Exit();

        } //Btn_Exit_Click


        internal void Exit()
        {
            Application.Exit();

        } // Exit


        private void ListeningThread() // let's listen in another thread instead!!
        {
            var port = 12345; // change as required

            listener = new TcpListener(IPAddress.Any, port);

            try
            {
                this.listener.Start();
            }
            catch (Exception e)
            {
                MessageBox.Show(@"couldn't bind to port " + port + @" -> " + e.Message); return;
            }

            while (true)
            {
                if (listener.Pending())
                    clients.Add(listener.AcceptSocket());  // won't block because pending was true

                foreach (var sock in clients)
                {
                    if (sock.Poll(0, SelectMode.SelectError))
                        clients.Remove(sock);
                    else if (sock.Poll(0, SelectMode.SelectRead))
                        ParserFunction(sock);
                }

                Thread.Sleep(30);
            }
        }

        private void ParserFunction(Socket sock)
        {
            var bytes = new Byte[512];

            var noBytes = sock.Receive(bytes);

            Console.WriteLine((char)bytes[0]);
        }
    }

}
