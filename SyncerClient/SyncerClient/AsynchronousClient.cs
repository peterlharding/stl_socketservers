﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace SyncerClient
{
    public class AsynchronousClient
    {
        // ManualResetEvent instances signal completion

        private static readonly ManualResetEvent connectDone = new ManualResetEvent(false);
        private static readonly ManualResetEvent sendDone = new ManualResetEvent(false);
        private static readonly ManualResetEvent receiveDone = new ManualResetEvent(false);

        // The response from the remote device

        private static string response = string.Empty;

        //-----------------------------------------------------------------------------------------

        public static void StartClient(string msg)
        {
            Socket client = null;

            try
            {
                // Establish the remote endpoint for the socket.
                // The name of the remote device is "host.contoso.com"

                var ipHostInfo = Dns.GetHostEntry(Globals.Hostname); // IPHostEntry
                var ipAddress = ipHostInfo.AddressList[0]; // IPAddress
                var remoteEP = new IPEndPoint(ipAddress, Globals.Port); // IPEndPoint

                // Create a TCP/IP socket

                client = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                // Connect to the remote endpoint

                client.BeginConnect(remoteEP, ConnectCallback, client);

                connectDone.WaitOne();

                // Send test data to the remote device

                Thread.Sleep(50);

                Send(client, $@"{msg}+");

                sendDone.WaitOne();

                // Receive the response from the remote device

                Receive(client);

                receiveDone.WaitOne();

                // Write the response to the console

                Console.WriteLine(@"Response received : {0}", response);

                // Release the socket

                client.Shutdown(SocketShutdown.Both);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                if (client != null)
                    client.Close();
                client = null;
            }

        } // StartClient

        //-----------------------------------------------------------------------------------------

        private static void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object

                var client = (Socket)ar.AsyncState;

                // Complete the connection

                client.EndConnect(ar);

                Console.WriteLine(@"Socket connected to {0}", client.RemoteEndPoint.ToString());

                // Signal that the connection has been made

                connectDone.Set();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

        } // ConnectCallback

        //-----------------------------------------------------------------------------------------

        private static void Receive(Socket client)
        {
            try
            {
                // Create the state object

                var state = new StateObject { WorkSocket = client };

                // Begin receiving the data from the remote device

                client.BeginReceive(state.Buffer, 0, StateObject.BufferSize, 0, ReceiveCallback, state);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

        } // Receive

        //-----------------------------------------------------------------------------------------

        private static void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the client socket from the asynchronous state object

                var state = (StateObject)ar.AsyncState;
                var client = state.WorkSocket;

                // Read data from the remote device
                var bytesRead = client.EndReceive(ar);

                if (bytesRead > 0)
                {
                    // There might be more data, so store the data received so far
                    state.Sb.Append(Encoding.ASCII.GetString(state.Buffer, 0, bytesRead));

                    // Get the rest of the data
                    client.BeginReceive(state.Buffer, 0, StateObject.BufferSize, 0,
                        new AsyncCallback(ReceiveCallback), state);
                }
                else
                {
                    // All the data has arrived; put it in response

                    if (state.Sb.Length > 1)
                    {
                        response = state.Sb.ToString();
                    }

                    // Signal that all bytes have been received

                    receiveDone.Set();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

        } // ReceiveCallback

        //-----------------------------------------------------------------------------------------

        private static void Send(Socket client, string data)
        {
            // Convert the string data to byte data using ASCII encoding

            var byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device

            client.BeginSend(byteData, 0, byteData.Length, 0, SendCallback, client);

        } // Send

        //-----------------------------------------------------------------------------------------

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object

                var client = (Socket)ar.AsyncState; // Socket

                // Complete sending the data to the remote device

                var bytesSent = client.EndSend(ar); // int

                Console.WriteLine(@"Sent {0} bytes to server.", bytesSent);

                // Signal that all bytes have been sent

                sendDone.Set();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

        } // SendCallback

        //-----------------------------------------------------------------------------------------

    }
}
