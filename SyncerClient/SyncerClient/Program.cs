﻿using System;

namespace SyncerClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(@"Testing...");

            var actions = new Args(args);

            Console.WriteLine(actions.Command);
            Console.WriteLine(actions.TraceLevel);

            if (actions.Command != @"None")
            {
                Console.WriteLine(actions.Command);
                AsynchronousClient.StartClient(actions.Command);
            }

        }
    }
}
