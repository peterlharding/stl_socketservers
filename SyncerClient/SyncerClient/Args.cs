﻿using System;
using System.Collections.Generic;

namespace SyncerClient
{
    public class Args
    {
        public string Command { get; set; }
        public int DebugLevel { get; set; }
        public int TraceLevel { get; set; }
        public int Verbosity { get; set; }
        public bool ShowHelp { get; set; }

        public List<string> Extras;

        //---------------------------------------------------------------------

        public Args(string[] args)
        {
            Command = @"None";

            // ReSharper disable once UnusedVariable
            var options = new OptionSet
            {
                { "c|command=", "Command\n", v => Command = v },
                { "h|help",  "Show this message and exit",  (bool v) => ShowHelp = v != false },
                { "D", "Increase debug level", v => { if (v != null) ++DebugLevel; } },
                { "T", "Increment TraceLevel\n",  v => { if (v != null) ++TraceLevel; } },
                { "v", "increase debug message verbosity", v => { if (v != null) ++Verbosity; } }
            };

            try
            {
                Extras = options.Parse(args);
            }
            catch (OptionException e)
            {
                Console.Write(@"Args: ");
                Console.WriteLine(e.Message);
                Console.WriteLine(@"Try `xxx --help' for more information.");
            }

        }  // Args

        //---------------------------------------------------------------------

    }  // class Args

    //-------------------------------------------------------------------------

}
