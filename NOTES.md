
MS SendArp()
============

* https://docs.microsoft.com/en-us/windows/desktop/api/iphlpapi/nf-iphlpapi-sendarp
* https://github.com/matugm/arp_scanner

Using ARP
=========

* https://www.guru99.com/ultimate-guide-to-network-sniffers.html



Notes
=====


From: https://codereview.stackexchange.com/questions/5709/c-net-getting-mac-address-of-a-local-nic-thats-making-a-connection-to-a-parti



 var address = addres.First(e => e.AddressFamily.Equals(AddressFamily.InterNetwork));
 
 // Retrieve routing table
 string output = string.Empty;
 using (Process p = new Process()
     {
         StartInfo = new ProcessStartInfo()
         {
             UseShellExecute = false,
             RedirectStandardOutput = true,
             WindowStyle = ProcessWindowStyle.Hidden,
             CreateNoWindow = false,
             FileName = "route",
             Arguments = "PRINT"
         }
     })
 {
     p.Start();
     output = p.StandardOutput.ReadToEnd();
     p.WaitForExit();
     p.Close();
 }
 
 // Find NIC interface from table
 var nicAddr = output.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
     .SkipWhile(e => !e.Equals("Active Routes:"))
     .Skip(1)
     .TakeWhile(e => !e.Equals("Active Routes:"))
     .TakeWhile(e => !e.StartsWith("="))
     .Where(e => !e.StartsWith("N"))
     .Select(e => new Route(e))
     .OrderByDescending(e => e.Mask)
     .ThenByDescending(e => e.Address)
     .First(e => e.IncludesHost(address))
     .ReadableInterface;
 
 // Get MAC address of associated IP address
 var macAddress = NetworkInterface.GetAllNetworkInterfaces()
     .Where(e => e.OperationalStatus.Equals(OperationalStatus.Up))
     .Where(e => e.GetIPProperties().UnicastAddresses
     .Where(a => a.Address.AddressFamily.Equals(AddressFamily.InterNetwork) &&
         a.Address.ToString().Equals(nicAddr)).FirstOrDefault() != null)
         .Select(a => a.GetPhysicalAddress()).FirstOrDefault();



ARP NOtes
=========

* https://erg.abdn.ac.uk/users/gorry/eg3567/inet-pages/arp.html
* https://en.wikipedia.org/wiki/Address_Resolution_Protocol



Using Ping and Network Discovery
================================


* https://forum.unity.com/threads/c-detecting-connected-devices-through-lan.297115/


 using UnityEngine;
 using System.Collections;
 using System.Collections.Generic;
 using System.Text;
 using System.Net;
 using System.Net.Sockets;
 using System.Net.NetworkInformation;
 
 public class LanManager {
     public List<string> _addresses { get; private set; }
 
     // Addresse that the ping will reach
     [HideInInspector] public string addressToBroadcast = "192.168.1.";
 
     public int timeout = 10;
 
     public bool _isSearching { get; private set; }
     public float _percentSearching { get; private set; }
 
     public int _portServer { get; private set; }
 
     public LanManager() {
         _addresses = new List<string>();
     }
 
     public IEnumerator ScanNetworkInterfaces() {
         foreach(NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
         {
                if(ni.OperationalStatus==OperationalStatus.Up) {
                 if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet) {
                        foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses) {
                            if (ip != null && ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                            {
                             // My addresses
                            }
 
                         yield return null;
                     }
                    }
             }
 
             yield return null;
         }
     }
 
     public IEnumerator SendRangePing(bool bClear = true) {
         if (bClear) {
             _addresses.Clear();
         }
 
         _isSearching = true;
         for (int i = 0 ; i < 255 ; i++) {
             string address = addressToBroadcast + i;
 
             SendPing(address);
 
             _percentSearching = (float)i / 255;
 
             yield return null;
         }
         _isSearching = false;
     }
 
     public void SendPing(string address) {
         System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
 
         // 128 TTL and DontFragment
         PingOptions pingOption = new PingOptions(16, true);
 
         // Once the ping has reached his target (or has timed out), call this function
         ping.PingCompleted += ping_PingCompleted;
 
         byte[] buffer = Encoding.ASCII.GetBytes("ping");
 
         try {
             // Do not block the main thread
             //ping.SendAsync(address, timeout, buffer, pingOption, addressToBroadcast);
             PingReply reply = ping.Send(address, timeout, buffer, pingOption);
 
             Debug.Log("Ping Sent at " + address);
 
             displayReply(reply);
         } catch (PingException ex) {
             Debug.Log(string.Format("Connection Error: {0}", ex.Message));
         } catch (SocketException ex) {
             Debug.Log(string.Format("Connection Error: {0}", ex.Message));
         }
     }
 
 
     private void ping_PingCompleted(object sender, PingCompletedEventArgs e) {
         string address = (string)e.UserState;
 
         if (e.Cancelled) {
             Debug.Log("Ping Canceled!");
         }
 
         if (e.Error != null) {
             Debug.Log(e.Error);
         }
 
         displayReply(e.Reply);
     }
 
     private void displayReply(PingReply reply) {
         if (reply != null) {
             Debug.Log("Status: " + reply.Status);
             if (reply.Status == IPStatus.Success) {
                 Debug.Log("Pong from " + reply.Address);
                 _addresses.Add(reply.Address.ToString());
 
                 if (NetworkManager.OnAddressFound != null) {
                     NetworkManager.OnAddressFound();
                 }
             }
         } else {
             Debug.Log("No reply");
         }
     }
 }




Wake on LAN
===========

* https://blog.cordiner.net/2010/03/06/wake-on-lan-c/



