﻿using System;
using System.Net;
using System.Reflection;
using System.Windows.Forms;

namespace AsyncSocketClient
{
    public partial class MainForm : Form
    {
        private readonly string _userDetails;

        //-----------------------------------------------------------------------------------------

        public MainForm()
        {
            InitializeComponent();

            var v = Assembly.GetExecutingAssembly().GetName().Version;

            Lbl_Main_Version.Text = $@"Version {v.Major}.{v.Minor}.{v.Build:00}.{v.Revision:00000}";

            Console.WriteLine(@"[Form_Main]  Initialization started");

            _userDetails = GetUserDetails();

        } // MainForm

        //-----------------------------------------------------------------------------------------

        //=========================================================================================
        // Form Handlers
        //-----------------------------------------------------------------------------------------

        #region - Form Handlers

        //-----------------------------------------------------------------------------------------

        private void MainForm_Load(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            // Invoke(new Action(() => { MessageBox.Show(this, @"Be patient. I am busy seeing whhat's what.  I'll be ready for you a little while!", @"PWI Starting"); }));

            RtbConsole.Setup(Rtb_Console);

            Cursor = Cursors.Default;

            Txt_Server.Text = Globals.Hostname;
            Txt_Port.Text = Globals.Port.ToString();

            var ipHostInfo = Dns.GetHostEntry(Globals.Hostname); // IPHostEntry
            var ipAddress = ipHostInfo.AddressList[0]; // IPAddress

            Txt_Address.Text = ipAddress.ToString();

        } // MainForm_Load

        //-----------------------------------------------------------------------------------------

        #endregion - Form Handlers

        //=========================================================================================

        #region - Button Handlers

        //-----------------------------------------------------------------------------------------

        private void Btn_Send_Click(object sender, EventArgs e)
        {
            Send();

        } // Btn_Send_Click

        //-----------------------------------------------------------------------------------------

        private void Btn_SyncTest_Click(object sender, EventArgs e)
        {
            Send(@"sync tests");

        } // Btn_SyncTest_Click

        //-----------------------------------------------------------------------------------------

        private void Btn_Exit_Click(object sender, EventArgs e)
        {
            Exit();

        } // Btn_Exit_Click

        //-----------------------------------------------------------------------------------------

        #endregion - Button Handlers

        //=========================================================================================

        #region - Menu handlers

        //-----------------------------------------------------------------------------------------
        // File Menu
        //-----------------------------------------------------------------------------------------

        private void Menu_File_Exit_Click(object sender, EventArgs e)
        {
            Exit();

        } // Menu_File_Exit_Click

        //-----------------------------------------------------------------------------------------
        // Edit Menu
        //-----------------------------------------------------------------------------------------

        private void Menu_Edit_Copy_Click(object sender, EventArgs e)
        {

        } // Menu_Edit_Copy_Click

        //-----------------------------------------------------------------------------------------

        private void Menu_Edit_Cut_Click(object sender, EventArgs e)
        {

        } // Menu_Edit_Cut_Click

        //-----------------------------------------------------------------------------------------

        private void Menu_Edit_Paste_Click(object sender, EventArgs e)
        {

        } // Menu_Edit_Paste_Click

        //-----------------------------------------------------------------------------------------

        private void Menu_Edit_Delete_Click(object sender, EventArgs e)
        {

        } // Menu_Edit_Delete_Click

        //-----------------------------------------------------------------------------------------
        // Sync Menu
        //-----------------------------------------------------------------------------------------

        private void Menu_Sync_Tests_Click(object sender, EventArgs e)
        {
            Send(@"sync tests");

        } // Menu_Sync_Tests_Click

        //-----------------------------------------------------------------------------------------
        
        private void Btn_SyncDate_Click(object sender, EventArgs e)
        {
            var timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            Send(timestamp);

        } // Btn_SyncDate_Click

        //-----------------------------------------------------------------------------------------

        private void Menu_Sync_Results_Click(object sender, EventArgs e)
        {
            Send(@"sync results");

        } // Menu_Sync_Results_Click

        //-----------------------------------------------------------------------------------------
        // Help Menu
        //-----------------------------------------------------------------------------------------

        private void Menu_Help_About_Click(object sender, EventArgs e)
        {

        } // Menu_Help_About_Click

        //-----------------------------------------------------------------------------------------

        private void Menu_Help_Help_Click(object sender, EventArgs e)
        {

        } // Menu_Help_Help_Click

        //-----------------------------------------------------------------------------------------

        #endregion - Menu handlers

        //=========================================================================================

        #region - Actions

        //-----------------------------------------------------------------------------------------

        internal void Send(string msg=null)
        {
            if (msg == null)
            {
                msg = Txt_Send.Text;
            }

            msg += Globals.EoT;

            RtbConsole.WriteLine($@"Sending |{msg}|");
            Console.WriteLine($@"Sending |{msg}|");

            AsynchronousClient.StartClient(msg);

        } // Send

        //-----------------------------------------------------------------------------------------

        internal void Help()
        {


        } // Exit

        //-----------------------------------------------------------------------------------------

        internal void About()
        {


        } // Exit

        //-----------------------------------------------------------------------------------------

        internal void Exit()
        {
            Application.Exit();

        } // Exit

        //-----------------------------------------------------------------------------------------

        #endregion - Actions

        //=========================================================================================
        // Classify and add to appropriate region
        //-----------------------------------------------------------------------------------------

        #region - Classify these

        //-----------------------------------------------------------------------------------------

        public string GetUserDetails()
        {
            var machineName = Environment.MachineName;

            var userDisplayName = System.DirectoryServices.AccountManagement.UserPrincipal.Current.DisplayName;

            var localUsername = Environment.UserName;

            var identity = System.Security.Principal.WindowsIdentity.GetCurrent();

            var networkUsername = identity.Name;

            Console.WriteLine(@"[Form_Main]  Initialization completed");
            Console.WriteLine(@"[Form_Main]  Machine Name {0}", machineName);
            Console.WriteLine(@"[Form_Main]  User Display Name {0}", userDisplayName);
            Console.WriteLine(@"[Form_Main]  Local Username {0}", localUsername);
            Console.WriteLine(@"[Form_Main]  Network Username {0}", networkUsername);

            return $@"{networkUsername ?? localUsername}@{machineName}";

        } // GetUserDetails







        //-----------------------------------------------------------------------------------------

        #endregion - Classify these

        //=========================================================================================


        //=========================================================================================

    }
}
