﻿namespace AsyncSocketClient
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Btn_Exit = new System.Windows.Forms.Button();
            this.Rtb_Console = new System.Windows.Forms.RichTextBox();
            this.Lbl_Send = new System.Windows.Forms.Label();
            this.Txt_Send = new System.Windows.Forms.TextBox();
            this.Btn_Send = new System.Windows.Forms.Button();
            this.Lbl_Main_Version = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.Menu_File = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_File_Open = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_File_Separator1 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_File_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Edit = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Edit_Copy = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Edit_Cut = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Edit_Paste = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Edit_Separator2 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_Edit_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_View = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Sync = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Sync_Tests = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Sync_Results = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Help = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Help_Help = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Help_Separator1 = new System.Windows.Forms.ToolStripSeparator();
            this.Menu_Help_About = new System.Windows.Forms.ToolStripMenuItem();
            this.Btn_SyncTest = new System.Windows.Forms.Button();
            this.Lbl_Server = new System.Windows.Forms.Label();
            this.Txt_Server = new System.Windows.Forms.TextBox();
            this.Txt_Address = new System.Windows.Forms.TextBox();
            this.Lbl_Address = new System.Windows.Forms.Label();
            this.Txt_Port = new System.Windows.Forms.TextBox();
            this.Lbl_Port = new System.Windows.Forms.Label();
            this.Btn_SyncDate = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Btn_Exit
            // 
            this.Btn_Exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Exit.Location = new System.Drawing.Point(713, 415);
            this.Btn_Exit.Name = "Btn_Exit";
            this.Btn_Exit.Size = new System.Drawing.Size(75, 23);
            this.Btn_Exit.TabIndex = 0;
            this.Btn_Exit.Text = "Exit";
            this.Btn_Exit.UseVisualStyleBackColor = true;
            this.Btn_Exit.Click += new System.EventHandler(this.Btn_Exit_Click);
            // 
            // Rtb_Console
            // 
            this.Rtb_Console.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Rtb_Console.Location = new System.Drawing.Point(12, 82);
            this.Rtb_Console.Name = "Rtb_Console";
            this.Rtb_Console.ReadOnly = true;
            this.Rtb_Console.Size = new System.Drawing.Size(776, 327);
            this.Rtb_Console.TabIndex = 1;
            this.Rtb_Console.Text = "";
            // 
            // Lbl_Send
            // 
            this.Lbl_Send.AutoSize = true;
            this.Lbl_Send.Location = new System.Drawing.Point(12, 59);
            this.Lbl_Send.Name = "Lbl_Send";
            this.Lbl_Send.Size = new System.Drawing.Size(32, 13);
            this.Lbl_Send.TabIndex = 2;
            this.Lbl_Send.Text = "Send";
            // 
            // Txt_Send
            // 
            this.Txt_Send.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Txt_Send.Location = new System.Drawing.Point(50, 56);
            this.Txt_Send.Name = "Txt_Send";
            this.Txt_Send.Size = new System.Drawing.Size(738, 20);
            this.Txt_Send.TabIndex = 3;
            // 
            // Btn_Send
            // 
            this.Btn_Send.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Send.Location = new System.Drawing.Point(632, 415);
            this.Btn_Send.Name = "Btn_Send";
            this.Btn_Send.Size = new System.Drawing.Size(75, 23);
            this.Btn_Send.TabIndex = 4;
            this.Btn_Send.Text = "Send";
            this.Btn_Send.UseVisualStyleBackColor = true;
            this.Btn_Send.Click += new System.EventHandler(this.Btn_Send_Click);
            // 
            // Lbl_Main_Version
            // 
            this.Lbl_Main_Version.AutoSize = true;
            this.Lbl_Main_Version.Location = new System.Drawing.Point(695, 9);
            this.Lbl_Main_Version.Name = "Lbl_Main_Version";
            this.Lbl_Main_Version.Size = new System.Drawing.Size(83, 13);
            this.Lbl_Main_Version.TabIndex = 5;
            this.Lbl_Main_Version.Text = "V0.0.000.00000";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_File,
            this.Menu_Edit,
            this.Menu_View,
            this.Menu_Sync,
            this.Menu_Help});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // Menu_File
            // 
            this.Menu_File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_File_Open,
            this.Menu_File_Separator1,
            this.Menu_File_Exit});
            this.Menu_File.Name = "Menu_File";
            this.Menu_File.Size = new System.Drawing.Size(37, 20);
            this.Menu_File.Text = "File";
            // 
            // Menu_File_Open
            // 
            this.Menu_File_Open.Name = "Menu_File_Open";
            this.Menu_File_Open.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.Menu_File_Open.Size = new System.Drawing.Size(146, 22);
            this.Menu_File_Open.Text = "Open";
            // 
            // Menu_File_Separator1
            // 
            this.Menu_File_Separator1.Name = "Menu_File_Separator1";
            this.Menu_File_Separator1.Size = new System.Drawing.Size(143, 6);
            // 
            // Menu_File_Exit
            // 
            this.Menu_File_Exit.Name = "Menu_File_Exit";
            this.Menu_File_Exit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.Menu_File_Exit.Size = new System.Drawing.Size(146, 22);
            this.Menu_File_Exit.Text = "Exit";
            this.Menu_File_Exit.Click += new System.EventHandler(this.Menu_File_Exit_Click);
            // 
            // Menu_Edit
            // 
            this.Menu_Edit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_Edit_Copy,
            this.Menu_Edit_Cut,
            this.Menu_Edit_Paste,
            this.Menu_Edit_Separator2,
            this.Menu_Edit_Delete});
            this.Menu_Edit.Name = "Menu_Edit";
            this.Menu_Edit.Size = new System.Drawing.Size(39, 20);
            this.Menu_Edit.Text = "Edit";
            // 
            // Menu_Edit_Copy
            // 
            this.Menu_Edit_Copy.Enabled = false;
            this.Menu_Edit_Copy.Name = "Menu_Edit_Copy";
            this.Menu_Edit_Copy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.Menu_Edit_Copy.Size = new System.Drawing.Size(144, 22);
            this.Menu_Edit_Copy.Text = "Copy";
            this.Menu_Edit_Copy.Click += new System.EventHandler(this.Menu_Edit_Copy_Click);
            // 
            // Menu_Edit_Cut
            // 
            this.Menu_Edit_Cut.Enabled = false;
            this.Menu_Edit_Cut.Name = "Menu_Edit_Cut";
            this.Menu_Edit_Cut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.Menu_Edit_Cut.Size = new System.Drawing.Size(144, 22);
            this.Menu_Edit_Cut.Text = "Cut";
            this.Menu_Edit_Cut.Click += new System.EventHandler(this.Menu_Edit_Cut_Click);
            // 
            // Menu_Edit_Paste
            // 
            this.Menu_Edit_Paste.Enabled = false;
            this.Menu_Edit_Paste.Name = "Menu_Edit_Paste";
            this.Menu_Edit_Paste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.Menu_Edit_Paste.Size = new System.Drawing.Size(144, 22);
            this.Menu_Edit_Paste.Text = "Paste";
            this.Menu_Edit_Paste.Click += new System.EventHandler(this.Menu_Edit_Paste_Click);
            // 
            // Menu_Edit_Separator2
            // 
            this.Menu_Edit_Separator2.Name = "Menu_Edit_Separator2";
            this.Menu_Edit_Separator2.Size = new System.Drawing.Size(141, 6);
            // 
            // Menu_Edit_Delete
            // 
            this.Menu_Edit_Delete.Enabled = false;
            this.Menu_Edit_Delete.Name = "Menu_Edit_Delete";
            this.Menu_Edit_Delete.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.Menu_Edit_Delete.Size = new System.Drawing.Size(144, 22);
            this.Menu_Edit_Delete.Text = "Delete";
            this.Menu_Edit_Delete.Click += new System.EventHandler(this.Menu_Edit_Delete_Click);
            // 
            // Menu_View
            // 
            this.Menu_View.Name = "Menu_View";
            this.Menu_View.Size = new System.Drawing.Size(44, 20);
            this.Menu_View.Text = "View";
            // 
            // Menu_Sync
            // 
            this.Menu_Sync.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_Sync_Tests,
            this.Menu_Sync_Results});
            this.Menu_Sync.Name = "Menu_Sync";
            this.Menu_Sync.Size = new System.Drawing.Size(44, 20);
            this.Menu_Sync.Text = "Sync";
            // 
            // Menu_Sync_Tests
            // 
            this.Menu_Sync_Tests.Name = "Menu_Sync_Tests";
            this.Menu_Sync_Tests.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.T)));
            this.Menu_Sync_Tests.Size = new System.Drawing.Size(175, 22);
            this.Menu_Sync_Tests.Text = "Tests";
            this.Menu_Sync_Tests.Click += new System.EventHandler(this.Menu_Sync_Tests_Click);
            // 
            // Menu_Sync_Results
            // 
            this.Menu_Sync_Results.Name = "Menu_Sync_Results";
            this.Menu_Sync_Results.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.R)));
            this.Menu_Sync_Results.Size = new System.Drawing.Size(175, 22);
            this.Menu_Sync_Results.Text = "Results";
            this.Menu_Sync_Results.Click += new System.EventHandler(this.Menu_Sync_Results_Click);
            // 
            // Menu_Help
            // 
            this.Menu_Help.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_Help_Help,
            this.Menu_Help_Separator1,
            this.Menu_Help_About});
            this.Menu_Help.Name = "Menu_Help";
            this.Menu_Help.Size = new System.Drawing.Size(44, 20);
            this.Menu_Help.Text = "Help";
            // 
            // Menu_Help_Help
            // 
            this.Menu_Help_Help.Name = "Menu_Help_Help";
            this.Menu_Help_Help.Size = new System.Drawing.Size(107, 22);
            this.Menu_Help_Help.Text = "Help";
            this.Menu_Help_Help.Click += new System.EventHandler(this.Menu_Help_Help_Click);
            // 
            // Menu_Help_Separator1
            // 
            this.Menu_Help_Separator1.Name = "Menu_Help_Separator1";
            this.Menu_Help_Separator1.Size = new System.Drawing.Size(104, 6);
            // 
            // Menu_Help_About
            // 
            this.Menu_Help_About.Name = "Menu_Help_About";
            this.Menu_Help_About.Size = new System.Drawing.Size(107, 22);
            this.Menu_Help_About.Text = "About";
            this.Menu_Help_About.Click += new System.EventHandler(this.Menu_Help_About_Click);
            // 
            // Btn_SyncTest
            // 
            this.Btn_SyncTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Btn_SyncTest.Location = new System.Drawing.Point(15, 415);
            this.Btn_SyncTest.Name = "Btn_SyncTest";
            this.Btn_SyncTest.Size = new System.Drawing.Size(75, 23);
            this.Btn_SyncTest.TabIndex = 7;
            this.Btn_SyncTest.Text = "Sync Test";
            this.Btn_SyncTest.UseVisualStyleBackColor = true;
            this.Btn_SyncTest.Click += new System.EventHandler(this.Btn_SyncTest_Click);
            // 
            // Lbl_Server
            // 
            this.Lbl_Server.AutoSize = true;
            this.Lbl_Server.Location = new System.Drawing.Point(12, 33);
            this.Lbl_Server.Name = "Lbl_Server";
            this.Lbl_Server.Size = new System.Drawing.Size(38, 13);
            this.Lbl_Server.TabIndex = 8;
            this.Lbl_Server.Text = "Server";
            // 
            // Txt_Server
            // 
            this.Txt_Server.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Txt_Server.Location = new System.Drawing.Point(56, 30);
            this.Txt_Server.Name = "Txt_Server";
            this.Txt_Server.Size = new System.Drawing.Size(154, 20);
            this.Txt_Server.TabIndex = 9;
            // 
            // Txt_Address
            // 
            this.Txt_Address.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Txt_Address.Location = new System.Drawing.Point(267, 30);
            this.Txt_Address.Name = "Txt_Address";
            this.Txt_Address.Size = new System.Drawing.Size(154, 20);
            this.Txt_Address.TabIndex = 11;
            // 
            // Lbl_Address
            // 
            this.Lbl_Address.AutoSize = true;
            this.Lbl_Address.Location = new System.Drawing.Point(216, 33);
            this.Lbl_Address.Name = "Lbl_Address";
            this.Lbl_Address.Size = new System.Drawing.Size(45, 13);
            this.Lbl_Address.TabIndex = 10;
            this.Lbl_Address.Text = "Address";
            // 
            // Txt_Port
            // 
            this.Txt_Port.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Txt_Port.Location = new System.Drawing.Point(459, 30);
            this.Txt_Port.Name = "Txt_Port";
            this.Txt_Port.Size = new System.Drawing.Size(154, 20);
            this.Txt_Port.TabIndex = 13;
            // 
            // Lbl_Port
            // 
            this.Lbl_Port.AutoSize = true;
            this.Lbl_Port.Location = new System.Drawing.Point(427, 33);
            this.Lbl_Port.Name = "Lbl_Port";
            this.Lbl_Port.Size = new System.Drawing.Size(26, 13);
            this.Lbl_Port.TabIndex = 12;
            this.Lbl_Port.Text = "Port";
            // 
            // Btn_SyncDate
            // 
            this.Btn_SyncDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Btn_SyncDate.Location = new System.Drawing.Point(96, 415);
            this.Btn_SyncDate.Name = "Btn_SyncDate";
            this.Btn_SyncDate.Size = new System.Drawing.Size(100, 23);
            this.Btn_SyncDate.TabIndex = 14;
            this.Btn_SyncDate.Text = "Send Timestamp";
            this.Btn_SyncDate.UseVisualStyleBackColor = true;
            this.Btn_SyncDate.Click += new System.EventHandler(this.Btn_SyncDate_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Btn_SyncDate);
            this.Controls.Add(this.Txt_Port);
            this.Controls.Add(this.Lbl_Port);
            this.Controls.Add(this.Txt_Address);
            this.Controls.Add(this.Lbl_Address);
            this.Controls.Add(this.Txt_Server);
            this.Controls.Add(this.Lbl_Server);
            this.Controls.Add(this.Btn_SyncTest);
            this.Controls.Add(this.Lbl_Main_Version);
            this.Controls.Add(this.Btn_Send);
            this.Controls.Add(this.Txt_Send);
            this.Controls.Add(this.Lbl_Send);
            this.Controls.Add(this.Rtb_Console);
            this.Controls.Add(this.Btn_Exit);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Async Socket Client";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_Exit;
        private System.Windows.Forms.RichTextBox Rtb_Console;
        private System.Windows.Forms.Label Lbl_Send;
        private System.Windows.Forms.TextBox Txt_Send;
        private System.Windows.Forms.Button Btn_Send;
        private System.Windows.Forms.Label Lbl_Main_Version;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem Menu_File;
        private System.Windows.Forms.ToolStripMenuItem Menu_File_Open;
        private System.Windows.Forms.ToolStripSeparator Menu_File_Separator1;
        private System.Windows.Forms.ToolStripMenuItem Menu_File_Exit;
        private System.Windows.Forms.ToolStripMenuItem Menu_Edit;
        private System.Windows.Forms.ToolStripMenuItem Menu_Edit_Copy;
        private System.Windows.Forms.ToolStripMenuItem Menu_Edit_Cut;
        private System.Windows.Forms.ToolStripMenuItem Menu_Edit_Paste;
        private System.Windows.Forms.ToolStripSeparator Menu_Edit_Separator2;
        private System.Windows.Forms.ToolStripMenuItem Menu_Edit_Delete;
        private System.Windows.Forms.ToolStripMenuItem Menu_View;
        private System.Windows.Forms.ToolStripMenuItem Menu_Help;
        private System.Windows.Forms.ToolStripMenuItem Menu_Help_Help;
        private System.Windows.Forms.ToolStripSeparator Menu_Help_Separator1;
        private System.Windows.Forms.ToolStripMenuItem Menu_Help_About;
        private System.Windows.Forms.Button Btn_SyncTest;
        private System.Windows.Forms.ToolStripMenuItem Menu_Sync;
        private System.Windows.Forms.ToolStripMenuItem Menu_Sync_Tests;
        private System.Windows.Forms.ToolStripMenuItem Menu_Sync_Results;
        private System.Windows.Forms.Label Lbl_Server;
        private System.Windows.Forms.TextBox Txt_Server;
        private System.Windows.Forms.TextBox Txt_Address;
        private System.Windows.Forms.Label Lbl_Address;
        private System.Windows.Forms.TextBox Txt_Port;
        private System.Windows.Forms.Label Lbl_Port;
        private System.Windows.Forms.Button Btn_SyncDate;
    }
}

