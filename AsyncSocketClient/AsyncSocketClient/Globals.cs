﻿namespace AsyncSocketClient
{
    class Globals
    {
        internal const string Hostname = @"SocketServer";
        internal const int Port = 11001;
        internal const string EoT = @"<EOT>";
    }
}
