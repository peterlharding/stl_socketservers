﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace AsyncSocketClient
{
    public class AsynchronousClient
    {
        // ManualResetEvent instances signal completion

        private static readonly ManualResetEvent connectDone = new ManualResetEvent(false);
        private static readonly ManualResetEvent sendDone = new ManualResetEvent(false);
        private static readonly ManualResetEvent receiveDone = new ManualResetEvent(false);

        // The response from the remote device

        private static string response = string.Empty;

        //-----------------------------------------------------------------------------------------

        public static void StartClient(string msg)
        {
            Socket client = null;

            try
            {
                // Establish the remote endpoint for the socket.
                // The name of the remote device is "host.contoso.com"

                var ipHostInfo = Dns.GetHostEntry(Globals.Hostname); // IPHostEntry
                var ipAddress = ipHostInfo.AddressList[0]; // IPAddress
                var remoteEP = new IPEndPoint(ipAddress, Globals.Port); // IPEndPoint

                // Create a TCP/IP socket

                client = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                // Connect to the remote endpoint

                client.BeginConnect(remoteEP, ConnectCallback, client);

                connectDone.WaitOne();

                // Send test data to the remote device

                Thread.Sleep(50);

                Send(client, $@"{msg}+");

                sendDone.WaitOne();

                // Receive the response from the remote device

                Receive(client);

                receiveDone.WaitOne();

                // Write the response to the console

                Console.WriteLine(@"Response received : {0}", response);

                // Release the socket

                client.Shutdown(SocketShutdown.Both);

            }
            catch (Exception e)
            {
                RtbConsole.WriteLine(@"===== StartClient =====");
                RtbConsole.WriteLine(e.ToString());
            }
            finally
            {
                client?.Close();
            }

        } // StartClient

        //-----------------------------------------------------------------------------------------

        private static void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object

                var client = (Socket)ar.AsyncState;

                // Complete the connection

                client.EndConnect(ar);

                Console.WriteLine(@"Socket connected to {0}", client.RemoteEndPoint);

                // Signal that the connection has been made

                connectDone.Set();
            }
            catch (Exception e)
            {
                Console.WriteLine(@"===== ConnectCallback =====");
                Console.WriteLine(e.ToString());
            }

        } // ConnectCallback

        //-----------------------------------------------------------------------------------------

        private static void Receive(Socket client)
        {
            try
            {
                // Create the state object

                var state = new StateObject {workSocket = client};

                // Begin receiving the data from the remote socket

                client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, ReceiveCallback, state);
            }
            catch (Exception e)
            {
                Console.WriteLine(@"===== Receive =====");
                Console.WriteLine(e.ToString());
            }

        } // Receive

        //-----------------------------------------------------------------------------------------

        private static void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the client socket from the asynchronous state object

                var state = (StateObject)ar.AsyncState;
                var client = state.workSocket;

                // Read data from the remote device

                var bytesRead = client.EndReceive(ar);

                if (bytesRead > 0)
                {
                    // Store the data received so far - there may be more to come

                    state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));

                    // Wait for more data

                    client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                        new AsyncCallback(ReceiveCallback), state);
                }
                else
                {
                    // No bytes read, so all the data has arrived; assign it to response

                    if (state.sb.Length > 1)
                    {
                        response = state.sb.ToString();
                    }

                    // Signal that all bytes have been received

                    receiveDone.Set();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(@"===== ReceiveCallback =====");
                Console.WriteLine(e.ToString());
            }

        } // ReceiveCallback

        //-----------------------------------------------------------------------------------------

        private static void Send(Socket client, string data)
        {
            // Convert the string to byte data using ASCII encoding

            Console.WriteLine(@"Sending |{0}|", data);

            var byteData = Encoding.ASCII.GetBytes(data);

            Console.WriteLine(@"Sending |{0}|", Encoding.Default.GetString(byteData));

            // Sending the data to the remote socket

            client.BeginSend(byteData, 0, byteData.Length, 0, SendCallback, client);

        } // Send

        //-----------------------------------------------------------------------------------------

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object

                var client = (Socket)ar.AsyncState; // Socket

                // Complete sending the data to the remote device

                var bytesSent = client.EndSend(ar); // int

                Console.WriteLine(@"Sent {0} bytes to server.", bytesSent);

                // Signal that all bytes have been sent

                sendDone.Set();
            }
            catch (Exception e)
            {
                Console.WriteLine(@"===== SendCallback =====");
                Console.WriteLine(e.ToString());
            }

        } // SendCallback

        //-----------------------------------------------------------------------------------------

    }
}
